package eu.telecomnancy.sensor;

import java.util.Random;

public abstract class SensorDecorator extends TemperatureSensor {
	protected TemperatureSensor tmps;
	 public void on() {
	    	
	        state = true;
	    }

	    @Override
	    public void off() {
	    	
	        state = false;
	    }

	    @Override
	    public boolean getStatus() {
	    	
	        return state;
	    }
	    public void update() throws SensorNotActivatedException{
	    	   if (state)
	               value = (new Random()).nextDouble() * 100;
	           else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	    }
	    public abstract double getValue() throws SensorNotActivatedException;

}
