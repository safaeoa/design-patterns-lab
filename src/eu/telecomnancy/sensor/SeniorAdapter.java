package eu.telecomnancy.sensor;

import java.util.Observable;

public class SeniorAdapter extends Observable implements ISensor {
	 private LegacyTemperatureSensor lts=new LegacyTemperatureSensor();
	 private double value;
	/**
    * Enable the sensor.
    */
   public void on(){
 
   	lts.onOff();
   	value=lts.getTemperature();
   	
   }

   /**
    * Disable the sensor.
    */
   public void off(){
   	
   	lts.onOff();
   }

   /**
    * Get the status (enabled/disabled) of the sensor.
    *
    * @return the current sensor's status.
    */
   public boolean getStatus(){
   	return lts.getStatus();
   }

   /**
    * Tell the sensor to acquire a new value.
    *
    * @throws SensorNotActivatedException if the sensor is not activated.
    */
   public void update() throws SensorNotActivatedException{
   	if (lts.getStatus()){
   		value=lts.getTemperature();
   		setChanged();
   		notifyObservers();
   	
   	}
       else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
   }
   

   /**
    * Get the latest value recorded by the sensor.
    *
    * @return the last recorded value.
    * @throws SensorNotActivatedException if the sensor is not activated.
    */
   public double getValue() throws SensorNotActivatedException{
   		    
   		if (lts.getStatus()){
   			setChanged();
   	   		notifyObservers();
   			return value;
   		}       
           else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    
   }

}

