package eu.telecomnancy.sensor;

public class Fahrenheit  extends SensorDecorator{



	@Override
	public double getValue() throws SensorNotActivatedException {
		 if (state)
	            return ((value*1.8)+32);
	        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	    }
	}


