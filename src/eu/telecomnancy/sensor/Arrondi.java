package eu.telecomnancy.sensor;

public class Arrondi extends SensorDecorator {

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (state)
            return Math.round(value);
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
